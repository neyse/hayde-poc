#!/usr/bin/env bash

HAYDE_CONFIG_FILEPATH=/vagrant/nginx/config.yml
HAYDE_DEST_DIR=/tmp/nginx-hayde-confs
NGINX_CONF_TEMP_DIR=/tmp/nginx-real-confs
NGINX_CONF_DIR=/etc/nginx/conf.d

rm -rf $HAYDE_DEST_DIR $NGINX_CONF_TEMP_DIR
mkdir $HAYDE_DEST_DIR $NGINX_CONF_TEMP_DIR
hayde --config-file $HAYDE_CONFIG_FILEPATH

if [ $? -eq 0 ]; then
    mv $NGINX_CONF_DIR/* $NGINX_CONF_TEMP_DIR 2> /dev/null
    cp $HAYDE_DEST_DIR/* $NGINX_CONF_DIR 2> /dev/null
    nginx -t
    if [ $? -eq 0 ]; then
        echo "Reload the nginx"
        nginx -s reload
    else
        mv $NGINX_CONF_TEMP_DIR/* $NGINX_CONF_DIR 2> /dev/null
        # Maybe slack message,
        # Maybe etckeeper
    fi
fi 