# -*- mode: ruby -*-
# vi: set ft=ruby :

HAYDE_VERSION = '0.1.2'

$install_common_tools = <<-SCRIPT
# bridged traffic to iptables is enabled for kube-router.
cat >> /etc/ufw/sysctl.conf <<EOF
net/bridge/bridge-nf-call-ip6tables = 1
net/bridge/bridge-nf-call-iptables = 1
net/bridge/bridge-nf-call-arptables = 1
EOF

# disable swap
swapoff -a
sed -i '/swap/d' /etc/fstab

# Install kubeadm, kubectl and kubelet
export DEBIAN_FRONTEND=noninteractive
apt-get -qq install ebtables ethtool
apt-get -qq update
apt-get -qq install -y docker.io apt-transport-https curl
cat <<EOF > /etc/docker/daemon.json
{
  "registry-mirrors": ["https://mirror.gcr.io"]
}
EOF
systemctl restart docker
systemctl enable docker.service
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get -qq update
apt-get -qq install -y kubelet kubeadm kubectl
SCRIPT

$provision_master_node = <<-SHELL
OUTPUT_FILE=/vagrant/join.sh
rm -rf $OUTPUT_FILE

# Start cluster
sudo kubeadm init --apiserver-advertise-address=10.0.0.10 --pod-network-cidr=10.244.0.0/16 | grep "kubeadm join" | awk '{print $0 "--discovery-token-unsafe-skip-ca-verification"}' > ${OUTPUT_FILE}
chmod +x $OUTPUT_FILE

# Configure kubectl
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Fix kubelet IP
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.10"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

# Configure flannel
curl -o kube-flannel.yml https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
sed -i.bak 's|"/opt/bin/flanneld",|"/opt/bin/flanneld", "--iface=enp0s8",|' kube-flannel.yml
kubectl create -f kube-flannel.yml

sudo systemctl daemon-reload
sudo systemctl restart kubelet
SHELL

$install_multicast = <<-SHELL
apt-get -qq install -y avahi-daemon libnss-mdns
SHELL

$install_hayde = <<-SHELL
apt update
apt install -y python3 python3-pip git
python3 -m pip install hayde==#{HAYDE_VERSION}
SHELL

$install_nginx = <<-SHELL
apt install -y nginx
cp /vagrant/nginx/nginx.conf /etc/nginx/nginx.conf
systemctl restart nginx
SHELL

Vagrant.configure('2') do |config|
  config.vm.box = 'ubuntu/focal64'

  config.vm.provider 'virtualbox' do |vb|
    vb.memory = 1024
    vb.cpus = 2
  end

  # config.vm.synced_folder '~/.kube', '/root/.kube' 
  config.vm.synced_folder '.', '/vagrant'


  config.vm.define :nginx do |n|
    n.vm.hostname = 'nginx'
    n.vm.network 'private_network', ip: '10.0.0.20'
    n.vm.provider 'virtualbox' do |vb|
      vb.memory = 1024
      vb.cpus = 1
    end
    n.vm.provision :shell, inline: $install_hayde
    n.vm.provision :shell, inline: $install_nginx
  end

  # config.vm.define "haproxy" do |h|
  #   h.vm.hostname = 'haproxy'
  #   h.vm.network "private_network", ip: '10.0.0.30'
  #   h.vm.provider "virtualbox" do |vb|
  #     vb.memory = "512"
  #     vb.cpus = "1"
  #   end
  #   h.vm.provision "shell", inline: <<-SHELL
  #     apt install -y haproxy
  #   SHELL
  # end

  config.vm.define :master do |master|
    master.vm.hostname = 'master'
    master.vm.network :private_network, ip: '10.0.0.10'
    master.vm.provision :shell, privileged: true, inline: $install_common_tools
    master.vm.provision :shell, privileged: false, inline: $provision_master_node
    master.vm.provision :shell, inline: $install_multicast
  end

  %w{worker1}.each_with_index do |name, i|
    config.vm.define name do |worker|
      worker.vm.hostname = name
      worker.vm.network :private_network, ip: "10.0.0.#{i + 11}"
      worker.vm.provision :shell, privileged: true, inline: $install_common_tools
      worker.vm.provision :shell, privileged: false, inline: <<-SHELL
sudo /vagrant/join.sh
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.#{i + 11}"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload
sudo systemctl restart kubelet
SHELL
      worker.vm.provision :shell, inline: $install_multicast
    end
  end
end
